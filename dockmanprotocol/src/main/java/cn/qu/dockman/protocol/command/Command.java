package cn.qu.dockman.protocol.command;

import cn.qu.dockman.protocol.def.Body;
import cn.qu.dockman.protocol.def.Header;

/**
 * Created by zh on 17/3/8.
 */
public abstract class Command<T extends Body> {

    private Header header;
    private T[] body;

    protected Command(Header header, T[] body) {
        this.header = header;
        this.body = body;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public T[] getBody() {
        return body;
    }

    public void setBody(T[] body) {
        this.body = body;
    }
}
