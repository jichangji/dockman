import cn.qu.dockman.protocol.command.client.MetricsCommand;
import com.google.gson.Gson;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zh on 17/3/11.
 */
public class GsonTest {

    @Test
    public void testGson() {
        MetricsCommand cmd = MetricsCommand.create("AAA", "111", 1);
        String json = new Gson().toJson(cmd);
        System.out.println(json);

        HashMap map = new Gson().fromJson(json, HashMap.class);
        Map header = (Map) map.get("header");
        System.out.println(header.get("name"));

        System.out.println(new Gson().fromJson(json, MetricsCommand.class).getClass());

    }
}
