package cn.test;

import cn.qu.dockman.protocol.command.client.MetricsCommand;
import com.google.gson.Gson;
import org.hyperic.sigar.SigarException;
import org.junit.Test;

/**
 * Created by zh on 17/3/9.
 */
public class SigarTest {

    @Test
    public void testCpu() throws SigarException {
        MetricsCommand cmd = MetricsCommand.create("name", "999", 1);
        System.out.println(cmd);
        Gson gson = new Gson();
        String json = gson.toJson(cmd, MetricsCommand.class);
        System.out.println(json);
    }
}
