package cn.qu.dockman.agent.scheduler.jobs;

import cn.qu.dockman.agent.scheduler.quartz.WsJob;
import cn.qu.dockman.agent.util.SigarUtils;
import cn.qu.dockman.protocol.command.client.MetricsCommand;
import org.hyperic.sigar.SigarException;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static cn.qu.dockman.protocol.command.client.MetricsCommand.MetricsBody;

/**
 * Created by zh on 17/3/16.
 */
public class HostJob extends WsJob {
    private static final Logger logger = LoggerFactory.getLogger(HostJob.class);

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        long cpuTotal = getCpuTotal();
        long cpuUsed = getCpuUsed();
        long memTotal = getMemTotal();
        long memUsed = getMemUsed();

        List<MetricsBody> bodyList = new ArrayList<>();

        if (cpuTotal != -1) {
            MetricsBody body = new MetricsBody();
            body.setName("CPU_TOTAL");
            body.setValue(String.valueOf(cpuTotal));
            body.setType(1);//1=主机
            bodyList.add(body);
        }
        if (cpuUsed != -1) {
            MetricsBody body = new MetricsBody();
            body.setName("CPU_USED");
            body.setValue(String.valueOf(cpuUsed));
            body.setType(1);//1=主机
            bodyList.add(body);
        }
        if (memTotal != -1) {
            MetricsBody body = new MetricsBody();
            body.setName("MEM_TOTAL");
            body.setValue(String.valueOf(memTotal));
            body.setType(1);//1=主机
            bodyList.add(body);
        }
        if (memUsed != -1) {
            MetricsBody body = new MetricsBody();
            body.setName("MEM_USED");
            body.setValue(String.valueOf(memUsed));
            body.setType(1);//1=主机
            bodyList.add(body);
        }
        try {
            if (!bodyList.isEmpty()) {
                getQueue().send(MetricsCommand.create(bodyList.toArray(new MetricsBody[0])));
            }
        } catch (Exception e) {
            logger.error("上传HOST指标失败");
        }

    }

    private long getMemTotal() {
        try {
            return SigarUtils.sigar.getMem().getTotal() / 1024;
        } catch (SigarException e) {
            logger.error("采集MEM_TOTAL指标异常", e);
            return -1;
        }
    }

    private long getMemUsed() {
        try {
            return Double.valueOf(SigarUtils.sigar.getMem().getUsedPercent() * 10000).longValue();
        } catch (SigarException e) {
            logger.error("采集MEM_TOTAL指标异常", e);
            return -1;
        }
    }

    private long getCpuTotal() {
        try {
            return SigarUtils.sigar.getCpu().getTotal();
        } catch (SigarException e) {
            logger.error("采集CPU_TOTAL指标异常", e);
            return -1;
        }
    }

    private long getCpuUsed() {
        try {
            return Double.valueOf(SigarUtils.sigar.getCpuPerc().getCombined() * 10000).longValue();
        } catch (SigarException e) {
            logger.error("采集CPU_USED指标异常", e);
            return -1;
        }
    }


}
