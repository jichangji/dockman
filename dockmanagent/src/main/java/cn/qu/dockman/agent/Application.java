package cn.qu.dockman.agent;

import cn.qu.dockman.agent.scheduler.HostScheduler;
import cn.qu.dockman.agent.scheduler.quartz.AutowiringSpringBeanJobFactory;
import cn.qu.dockman.agent.util.DockmanClient;
import cn.qu.dockman.agent.ws.WebSocket;
import org.quartz.Trigger;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import java.io.IOException;
import java.net.URI;

/**
 * Created by zh on 17/3/8.
 */
@SpringBootApplication
@Import({HostScheduler.class})
public class Application {

    @Bean
    public JobFactory jobFactory(ApplicationContext applicationContext) {
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(
            JobFactory jobFactory,
            @Qualifier("pingJobTrigger") Trigger pingJobTrigger,
            @Qualifier("hostJobTrigger") Trigger hostJobTrigger,
            @Qualifier("imageListJobTrigger") Trigger imageListJobTrigger,
            @Qualifier("containerListJobTrigger") Trigger containerListJobTrigger,
            @Qualifier("networkJobTrigger") Trigger networkJobTrigger
    ) throws IOException {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        factory.setOverwriteExistingJobs(true);
        factory.setJobFactory(jobFactory);
        factory.setStartupDelay(10);
        factory.setTriggers(pingJobTrigger, hostJobTrigger, imageListJobTrigger, containerListJobTrigger, networkJobTrigger);

        return factory;
    }

    @Bean
    @ConfigurationProperties(prefix = "docker")
    public DockmanClient.DockerConfig createDockerConfig() {
        return new DockmanClient.DockerConfig();
    }

    @Bean
    public DockmanClient createDockmanClient(DockmanClient.DockerConfig dockerConfig) {
        return new DockmanClient(dockerConfig);
    }

    @Bean
    public ServerEndpointExporter createServerEndpointExporter() {
        return new ServerEndpointExporter();
    }

    @Bean
    public WebSocket createWebSocket(@Value("${token}") String token, DockmanClient dockmanClient, @Value("${server.ws}") URI serverWs) {
        WebSocket webSocket = new WebSocket(serverWs, dockmanClient);
        webSocket.setToken(token);
        return webSocket;
    }


    public static void main(String[] args) {
        new SpringApplication(Application.class).run(args);
    }
}