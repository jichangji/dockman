package cn.qu.dockman.agent.scheduler.quartz;

import cn.qu.dockman.agent.util.SendingQueue;
import org.quartz.Job;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by zh on 17/3/16.
 */
public abstract class WsJob implements Job {

    @Autowired
    private SendingQueue sendingQueue;


    public SendingQueue getQueue() {
        return sendingQueue;
    }
}
