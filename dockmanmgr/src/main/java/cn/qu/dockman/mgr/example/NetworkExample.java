package cn.qu.dockman.mgr.example;

import java.util.ArrayList;
import java.util.List;

public class NetworkExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public NetworkExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNetworkIdIsNull() {
            addCriterion("network_id is null");
            return (Criteria) this;
        }

        public Criteria andNetworkIdIsNotNull() {
            addCriterion("network_id is not null");
            return (Criteria) this;
        }

        public Criteria andNetworkIdEqualTo(String value) {
            addCriterion("network_id =", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdNotEqualTo(String value) {
            addCriterion("network_id <>", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdGreaterThan(String value) {
            addCriterion("network_id >", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdGreaterThanOrEqualTo(String value) {
            addCriterion("network_id >=", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdLessThan(String value) {
            addCriterion("network_id <", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdLessThanOrEqualTo(String value) {
            addCriterion("network_id <=", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdLike(String value) {
            addCriterion("network_id like", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdNotLike(String value) {
            addCriterion("network_id not like", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdIn(List<String> values) {
            addCriterion("network_id in", values, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdNotIn(List<String> values) {
            addCriterion("network_id not in", values, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdBetween(String value1, String value2) {
            addCriterion("network_id between", value1, value2, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdNotBetween(String value1, String value2) {
            addCriterion("network_id not between", value1, value2, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkNameIsNull() {
            addCriterion("network_name is null");
            return (Criteria) this;
        }

        public Criteria andNetworkNameIsNotNull() {
            addCriterion("network_name is not null");
            return (Criteria) this;
        }

        public Criteria andNetworkNameEqualTo(String value) {
            addCriterion("network_name =", value, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkNameNotEqualTo(String value) {
            addCriterion("network_name <>", value, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkNameGreaterThan(String value) {
            addCriterion("network_name >", value, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkNameGreaterThanOrEqualTo(String value) {
            addCriterion("network_name >=", value, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkNameLessThan(String value) {
            addCriterion("network_name <", value, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkNameLessThanOrEqualTo(String value) {
            addCriterion("network_name <=", value, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkNameLike(String value) {
            addCriterion("network_name like", value, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkNameNotLike(String value) {
            addCriterion("network_name not like", value, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkNameIn(List<String> values) {
            addCriterion("network_name in", values, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkNameNotIn(List<String> values) {
            addCriterion("network_name not in", values, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkNameBetween(String value1, String value2) {
            addCriterion("network_name between", value1, value2, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkNameNotBetween(String value1, String value2) {
            addCriterion("network_name not between", value1, value2, "networkName");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverIsNull() {
            addCriterion("network_driver is null");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverIsNotNull() {
            addCriterion("network_driver is not null");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverEqualTo(String value) {
            addCriterion("network_driver =", value, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverNotEqualTo(String value) {
            addCriterion("network_driver <>", value, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverGreaterThan(String value) {
            addCriterion("network_driver >", value, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverGreaterThanOrEqualTo(String value) {
            addCriterion("network_driver >=", value, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverLessThan(String value) {
            addCriterion("network_driver <", value, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverLessThanOrEqualTo(String value) {
            addCriterion("network_driver <=", value, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverLike(String value) {
            addCriterion("network_driver like", value, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverNotLike(String value) {
            addCriterion("network_driver not like", value, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverIn(List<String> values) {
            addCriterion("network_driver in", values, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverNotIn(List<String> values) {
            addCriterion("network_driver not in", values, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverBetween(String value1, String value2) {
            addCriterion("network_driver between", value1, value2, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkDriverNotBetween(String value1, String value2) {
            addCriterion("network_driver not between", value1, value2, "networkDriver");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeIsNull() {
            addCriterion("network_scope is null");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeIsNotNull() {
            addCriterion("network_scope is not null");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeEqualTo(String value) {
            addCriterion("network_scope =", value, "networkScope");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeNotEqualTo(String value) {
            addCriterion("network_scope <>", value, "networkScope");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeGreaterThan(String value) {
            addCriterion("network_scope >", value, "networkScope");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeGreaterThanOrEqualTo(String value) {
            addCriterion("network_scope >=", value, "networkScope");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeLessThan(String value) {
            addCriterion("network_scope <", value, "networkScope");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeLessThanOrEqualTo(String value) {
            addCriterion("network_scope <=", value, "networkScope");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeLike(String value) {
            addCriterion("network_scope like", value, "networkScope");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeNotLike(String value) {
            addCriterion("network_scope not like", value, "networkScope");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeIn(List<String> values) {
            addCriterion("network_scope in", values, "networkScope");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeNotIn(List<String> values) {
            addCriterion("network_scope not in", values, "networkScope");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeBetween(String value1, String value2) {
            addCriterion("network_scope between", value1, value2, "networkScope");
            return (Criteria) this;
        }

        public Criteria andNetworkScopeNotBetween(String value1, String value2) {
            addCriterion("network_scope not between", value1, value2, "networkScope");
            return (Criteria) this;
        }

        public Criteria andHostIdIsNull() {
            addCriterion("host_id is null");
            return (Criteria) this;
        }

        public Criteria andHostIdIsNotNull() {
            addCriterion("host_id is not null");
            return (Criteria) this;
        }

        public Criteria andHostIdEqualTo(Integer value) {
            addCriterion("host_id =", value, "hostId");
            return (Criteria) this;
        }

        public Criteria andHostIdNotEqualTo(Integer value) {
            addCriterion("host_id <>", value, "hostId");
            return (Criteria) this;
        }

        public Criteria andHostIdGreaterThan(Integer value) {
            addCriterion("host_id >", value, "hostId");
            return (Criteria) this;
        }

        public Criteria andHostIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("host_id >=", value, "hostId");
            return (Criteria) this;
        }

        public Criteria andHostIdLessThan(Integer value) {
            addCriterion("host_id <", value, "hostId");
            return (Criteria) this;
        }

        public Criteria andHostIdLessThanOrEqualTo(Integer value) {
            addCriterion("host_id <=", value, "hostId");
            return (Criteria) this;
        }

        public Criteria andHostIdIn(List<Integer> values) {
            addCriterion("host_id in", values, "hostId");
            return (Criteria) this;
        }

        public Criteria andHostIdNotIn(List<Integer> values) {
            addCriterion("host_id not in", values, "hostId");
            return (Criteria) this;
        }

        public Criteria andHostIdBetween(Integer value1, Integer value2) {
            addCriterion("host_id between", value1, value2, "hostId");
            return (Criteria) this;
        }

        public Criteria andHostIdNotBetween(Integer value1, Integer value2) {
            addCriterion("host_id not between", value1, value2, "hostId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}