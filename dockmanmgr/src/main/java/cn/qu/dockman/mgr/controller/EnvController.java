package cn.qu.dockman.mgr.controller;

import cn.qu.dockman.mgr.entity.Env;
import cn.qu.dockman.mgr.resp.EnvBean;
import cn.qu.dockman.mgr.service.EnvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by zh on 17/3/7.
 */
@RestController
@RequestMapping("/env")
public class EnvController {

    @Autowired
    private EnvService envService;

    @RequestMapping(method = RequestMethod.GET)
    public List<EnvBean> findAll() {

        return envService.findEnvs();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Env findById(@PathVariable int id) {
        return envService.findById(id);
    }
}
