package cn.qu.dockman.mgr.ws.exec;

import cn.qu.dockman.mgr.disparte.ArgDef;
import cn.qu.dockman.mgr.disparte.DispartMgr;
import cn.qu.dockman.mgr.entity.Host;
import cn.qu.dockman.mgr.entity.Metrics;
import cn.qu.dockman.mgr.service.HostService;
import cn.qu.dockman.mgr.service.MetricsService;
import cn.qu.dockman.protocol.command.Command;
import cn.qu.dockman.protocol.command.client.MetricsCommand;

import java.util.Date;

/**
 * Created by zh on 17/3/16.
 */
public class MetricsCommandParser extends CommandParser {

    private MetricsService metricsService;
    private DispartMgr dispartMgr;
    private HostService hostService;

    @Override
    public void before() {
        this.metricsService = getApplicationContext().getBean(MetricsService.class);
        this.dispartMgr = getApplicationContext().getBean(DispartMgr.class);
        this.hostService = getApplicationContext().getBean(HostService.class);
    }

    @Override
    public void parse(Command command) {

        Host host = hostService.findByIp(this.getHost());

        MetricsCommand cmd = (MetricsCommand) command;

        if (host != null) {
            MetricsCommand.MetricsBody[] bodys = cmd.getBody();
            for (MetricsCommand.MetricsBody body : bodys) {
                Metrics metrics = new Metrics();
                metrics.setMetricsName(body.getName());
                metrics.setMetricsValue(body.getValue());
                metrics.setMetricsTime(new Date());
                metrics.setMoId(host.getId());
                metrics.setMoType(body.getType());
                metricsService.save(metrics);

                ArgDef def = new ArgDef(metrics.getMetricsName(), this.getHost(), this.getPort(), this.getHostname());
                def.setData(metrics);

                dispartMgr.notifyObservers(def);
            }
        }

    }
}
