package cn.qu.dockman.mgr.disparte;


/**
 * Created by zh on 17/3/16.
 */
public class ArgDef {
    private String name;
    private String host;
    private String hostname;
    private int port;
    private Object data;

    public ArgDef(String name, String host, int port, String hostname) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.hostname = hostname;
    }

    public String getName() {
        return name;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public String getHostname() {
        return hostname;
    }
}
