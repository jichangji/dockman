package cn.qu.dockman.mgr.ws.exec;

import cn.qu.dockman.protocol.command.Command;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Created by zh on 17/3/16.
 */
public abstract class CommandParser implements ApplicationContextAware {

    private ApplicationContext applicationContext;
    private String host;
    private int port;
    private String hostname;

    public void before() {

    }
    public abstract void parse(Command command);

    @Override
    final public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
}
