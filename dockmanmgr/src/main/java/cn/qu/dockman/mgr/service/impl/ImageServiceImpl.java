package cn.qu.dockman.mgr.service.impl;

import cn.qu.dockman.mgr.entity.Image;
import cn.qu.dockman.mgr.example.ImageExample;
import cn.qu.dockman.mgr.mapper.ImageMapper;
import cn.qu.dockman.mgr.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zh on 17/3/10.
 */
@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    private ImageMapper imageMapper;

    @Override
    public List<Image> findByHostId(int hostId) {
        List<Image> hostList = imageMapper.selectByExample(new ImageExample() {{
            createCriteria()
                    .andHostIdEqualTo(hostId);
        }});
        return hostList;
    }

    @Override
    public Image findByHostIdAndImageId(int hostId, String imageId) {
        List<Image> hostList = imageMapper.selectByExample(new ImageExample() {{
            createCriteria()
                    .andHostIdEqualTo(hostId)
                    .andImageIdEqualTo(imageId);
        }});
        if (hostList == null || hostList.isEmpty()) {
            return null;
        } else {
            return hostList.get(0);
        }
    }

    @Override
    public void saveOrUpdate(Image image) {
        if (image.getId() == null) {
            imageMapper.insert(image);
        } else {
            imageMapper.updateByPrimaryKey(image);
        }
    }

    @Override
    public void delete(Image image) {
        imageMapper.deleteByPrimaryKey(image.getId());
    }

}
