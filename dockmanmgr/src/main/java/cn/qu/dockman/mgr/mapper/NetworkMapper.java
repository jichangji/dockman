package cn.qu.dockman.mgr.mapper;

import cn.qu.dockman.mgr.entity.Network;
import cn.qu.dockman.mgr.example.NetworkExample;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface NetworkMapper {
    long countByExample(NetworkExample example);

    int deleteByExample(NetworkExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Network record);

    int insertSelective(Network record);

    List<Network> selectByExample(NetworkExample example);

    Network selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Network record, @Param("example") NetworkExample example);

    int updateByExample(@Param("record") Network record, @Param("example") NetworkExample example);

    int updateByPrimaryKeySelective(Network record);

    int updateByPrimaryKey(Network record);
}