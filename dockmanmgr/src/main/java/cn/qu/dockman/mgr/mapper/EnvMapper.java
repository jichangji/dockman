package cn.qu.dockman.mgr.mapper;

import cn.qu.dockman.mgr.entity.Env;
import cn.qu.dockman.mgr.example.EnvExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EnvMapper {
    long countByExample(EnvExample example);

    int deleteByExample(EnvExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Env record);

    int insertSelective(Env record);

    List<Env> selectByExample(EnvExample example);

    Env selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Env record, @Param("example") EnvExample example);

    int updateByExample(@Param("record") Env record, @Param("example") EnvExample example);

    int updateByPrimaryKeySelective(Env record);

    int updateByPrimaryKey(Env record);
}