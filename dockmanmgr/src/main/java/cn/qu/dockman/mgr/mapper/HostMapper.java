package cn.qu.dockman.mgr.mapper;

import cn.qu.dockman.mgr.entity.Host;
import cn.qu.dockman.mgr.example.HostExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface HostMapper {
    long countByExample(HostExample example);

    int deleteByExample(HostExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Host record);

    int insertSelective(Host record);

    List<Host> selectByExample(HostExample example);

    Host selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Host record, @Param("example") HostExample example);

    int updateByExample(@Param("record") Host record, @Param("example") HostExample example);

    int updateByPrimaryKeySelective(Host record);

    int updateByPrimaryKey(Host record);
}