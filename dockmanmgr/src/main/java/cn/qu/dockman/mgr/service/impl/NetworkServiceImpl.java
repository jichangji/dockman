package cn.qu.dockman.mgr.service.impl;

import cn.qu.dockman.mgr.entity.Network;
import cn.qu.dockman.mgr.example.NetworkExample;
import cn.qu.dockman.mgr.mapper.NetworkMapper;
import cn.qu.dockman.mgr.service.NetworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zh on 17/3/17.
 */
@Service
public class NetworkServiceImpl implements NetworkService {

    @Autowired
    private NetworkMapper networkMapper;

    @Override
    public Network findNetwork(int hostId, String networkId) {
        List<Network> networkList = networkMapper.selectByExample(new NetworkExample() {{
            createCriteria().andHostIdEqualTo(hostId)
                    .andNetworkIdEqualTo(networkId);
        }});
        if (networkList == null || networkList.isEmpty()) {
            return null;
        } else {
            return networkList.get(0);
        }

    }

    @Override
    public void saveOrUpdate(Network network) {
        if (network.getId() == null) {
            networkMapper.insert(network);
        } else {
            networkMapper.updateByPrimaryKey(network);
        }
    }

    @Override
    public List<Network> findByHostId(int hostId) {
        return networkMapper.selectByExample(new NetworkExample() {{
            createCriteria().andHostIdEqualTo(hostId);
        }});
    }

    @Override
    public void delete(Network network) {
        networkMapper.deleteByPrimaryKey(network.getId());
    }
}
