package cn.qu.dockman.mgr.service;

import cn.qu.dockman.mgr.entity.Image;

import java.util.List;

/**
 * Created by zh on 17/3/10.
 */
public interface ImageService {
    List<Image> findByHostId(int hostId);

    Image findByHostIdAndImageId(int hostId, String imageId);

    void saveOrUpdate(Image image);

    void delete(Image image);
}
