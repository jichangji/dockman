package cn.qu.dockman.mgr.service;

import cn.qu.dockman.mgr.entity.Metrics;

import java.util.Date;
import java.util.List;

/**
 * Created by zh on 17/3/14.
 */
public interface MetricsService {
    void save(Metrics metrics);

    List<Metrics> findByName(String name,int moId,Date date);

}
