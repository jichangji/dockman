package cn.qu.dockman.mgr.service;

import cn.qu.dockman.mgr.entity.Env;
import cn.qu.dockman.mgr.resp.EnvBean;

import java.util.List;

/**
 * Created by zh on 17/3/7.
 */
public interface EnvService {
    List<EnvBean> findEnvs();
    Env findById(int id);
}
