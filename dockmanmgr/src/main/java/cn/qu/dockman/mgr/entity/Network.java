package cn.qu.dockman.mgr.entity;

public class Network {
    private Integer id;

    private String networkId;

    private String networkName;

    private String networkDriver;

    private String networkScope;

    private Integer hostId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId == null ? null : networkId.trim();
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName == null ? null : networkName.trim();
    }

    public String getNetworkDriver() {
        return networkDriver;
    }

    public void setNetworkDriver(String networkDriver) {
        this.networkDriver = networkDriver == null ? null : networkDriver.trim();
    }

    public String getNetworkScope() {
        return networkScope;
    }

    public void setNetworkScope(String networkScope) {
        this.networkScope = networkScope == null ? null : networkScope.trim();
    }

    public Integer getHostId() {
        return hostId;
    }

    public void setHostId(Integer hostId) {
        this.hostId = hostId;
    }
}