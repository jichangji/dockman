package cn.qu.dockman.mgr.service.impl;

import cn.qu.dockman.mgr.entity.Metrics;
import cn.qu.dockman.mgr.example.MetricsExample;
import cn.qu.dockman.mgr.mapper.MetricsMapper;
import cn.qu.dockman.mgr.service.MetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by zh on 17/3/14.
 */
@Service
public class MetricsServiceImpl implements MetricsService {
    @Autowired
    private MetricsMapper metricsMapper;

    @Override
    public void save(Metrics metrics) {
        metricsMapper.insert(metrics);
    }

    @Override
    public List<Metrics> findByName(String name, int moId, Date date) {
        List<Metrics> metricsList = metricsMapper.selectByExample(new MetricsExample() {{
            Criteria criteria = createCriteria().andMetricsNameEqualTo(name).andMoIdEqualTo(moId);
            if (date != null) {
                criteria.andMetricsTimeGreaterThan(date);

            }
            setOrderByClause("metrics_time desc");
        }});
        return metricsList;
    }
}
