/**
 * Created by zh on 17/3/7.
 */

angular.module('dockman.host.controller', ['echarts', 'dockman.host.service', 'dockman.env.service', 'dockman.image.service', 'dockman.container.service', 'dockman.network.service', 'dockman.metrics.service'])
    .controller('hostTopController', ['$scope', '$routeParams', '$location', '$interval', 'HostResource', 'EnvResource',
        function ($scope, $routeParams, $location, $interval, HostResource, EnvResource) {
            $scope.page = {
                pageNo: 1,
                pageSize: 10
            };
            $scope.maxSize = 10;
            $scope.total = 0;
            $scope.items = [];
            $scope.env = {};


            EnvResource.get({id: $routeParams.envId}, function (data) {
                $scope.env = data;
            });

            $scope.pageChanged = function () {
                HostResource.get(angular.extend($scope.page, {id: $routeParams.envId}), function (data) {
                    $scope.total = data.total;
                    $scope.items = data.list;
                });
            };

            $scope.info = function (id) {
                $location.path('/host/info/' + id);
            };

            $interval($scope.pageChanged, 1000);

            $scope.pageChanged();


        }])
    .controller('hostInfoController', ['$scope', '$routeParams', 'HostResource', function ($scope, $routeParams, HostResource) {
        $scope.host = {};

        HostResource.gethost({id: $routeParams.id}, function (data) {
            $scope.host = data;
        });

        $scope.activeMe = function () {
            $scope.$broadcast('type', {
                name: 'monitor',
                host: $scope.host
            });
        };
    }])
    .controller('settingController', ['$scope', function ($scope) {

    }])
    .controller('imageController', ['$scope', '$routeParams', '$interval', 'ImageResource',
        function ($scope, $routeParams, $interval, ImageResource) {
            $scope.page = {
                pageNo: 1,
                pageSize: 10
            };
            $scope.maxSize = 10;
            $scope.total = 0;
            $scope.items = [];
            $scope.env = {};

            ImageResource.get({id: $routeParams.id}, function (data) {
                $scope.env = data;
            });

            $scope.pageChanged = function () {
                ImageResource.get(angular.extend($scope.page, {id: $routeParams.id}), function (data) {
                    $scope.total = data.total;
                    $scope.items = data.list;
                });
            };

            $scope.info = function (id) {
                $location.path('/host/info/' + id);
            };

            $scope.pageChanged();
            $interval($scope.pageChanged, 1000);
        }
    ])

    .controller('containerStartController', ['$scope', '$uibModalInstance', 'ContainerResource', 'item',
        function ($scope, $uibModalInstance, ContainerResource, item) {
            $scope.title = '确认提示';
            $scope.body = '你确定要启动容器吗？';
            $scope.ok = function () {
                ContainerResource.start({hostId: item.hostId, containerId: item.containerId}, function (data) {
                    $uibModalInstance.close();
                });
            };
            $scope.cancel = function () {
                $uibModalInstance.close();
            };
        }])

    .controller('containerStopController', ['$scope', '$uibModalInstance', 'ContainerResource', 'item',
        function ($scope, $uibModalInstance, ContainerResource, item) {
            $scope.title = '确认提示';
            $scope.body = '你确定要停止容器吗？';
            $scope.ok = function () {
                ContainerResource.stop({hostId: item.hostId, containerId: item.containerId}, function (data) {
                    $uibModalInstance.close();
                });
            };
            $scope.cancel = function () {
                $uibModalInstance.close();
            };
        }])
    .controller('containerController', ['$scope', '$routeParams', '$interval', '$uibModal', 'ContainerResource',
        function ($scope, $routeParams, $interval, $uibModal, ContainerResource) {
            $scope.page = {
                pageNo: 1,
                pageSize: 10
            };
            $scope.maxSize = 10;
            $scope.total = 0;
            $scope.items = [];
            $scope.env = {};

            $scope.pageChanged = function () {
                ContainerResource.get(angular.extend($scope.page, {id: $routeParams.id}), function (data) {
                    $scope.total = data.total;
                    $scope.items = data.list;
                });
            };
            $scope.pageChanged();
            $interval($scope.pageChanged, 1000);

            $scope.start = function (containerId) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'modal-confirm',
                    controller: 'containerStartController',
                    size: 'sm',
                    resolve: {
                        item: function () {
                            return {
                                hostId: $routeParams.id,
                                containerId: containerId
                            }
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                }, function () {
                });

            };
            $scope.stop = function (containerId) {

                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'modal-confirm',
                    controller: 'containerStopController',
                    size: 'sm',
                    resolve: {
                        item: function () {
                            return {
                                hostId: $routeParams.id,
                                containerId: containerId
                            }
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                }, function () {
                });
            };
        }
    ])

    .controller('networkController', ['$scope', '$routeParams', '$interval', 'NetworkResource',
        function ($scope, $routeParams, $interval, NetworkResource) {
            $scope.page = {
                pageNo: 1,
                pageSize: 10
            };
            $scope.maxSize = 10;
            $scope.total = 0;
            $scope.items = [];
            $scope.env = {};

            $scope.pageChanged = function () {
                NetworkResource.get(angular.extend($scope.page, {id: $routeParams.id}), function (data) {
                    $scope.total = data.total;
                    $scope.items = data.list;
                });
            };
            $scope.pageChanged();
            $interval($scope.pageChanged, 1000);
        }
    ])
    .controller('cpuMonitorController', ['$scope', '$timeout', '$interval', 'MetricsResource', function ($scope, $timeout, $interval, MetricsResource) {
        $scope.legend = 'CPU使用率';
        $scope.item = [];
        $scope.data = [];

        var option = {
            // 提示框，鼠标悬浮交互时的信息提示
            tooltip: {
                trigger: 'axis',
                formatter: function (params) {
                    return params[0].name + '\n' + params[0].seriesName + ':' + params[0].value + '%';
                },
                axisPointer: {
                    animation: true
                }
            },
            // 横轴坐标轴
            xAxis: [{
                type: 'category',
                data: $scope.item,
                splitLine: {
                    show: false
                }
            }],
            // 纵轴坐标轴
            yAxis: [{
                type: 'value',
                // boundaryGap: [0, '100%'],
                splitLine: {
                    show: false
                }
            }],
            series: {
                name: $scope.legend,
                type: 'line',
                showSymbol: false,
                hoverAnimation: false
            }
        };
        var myChart = echarts.init(document.getElementById('cpu'), 'macarons');

        myChart.setOption(option);

        $scope.start = '';

        $scope.loadData = function (param) {
            MetricsResource.query(param, function (data) {
                for (var i in data) {
                    if (i != '$promise' && i != '$resolved') {
                        if ($scope.data.length == 50) {
                            $scope.data.shift();
                            $scope.item.shift();
                        }
                        $scope.data.push(data[i].metricsValue / 100);
                        $scope.item.push(data[i].metricsTime);
                        if (i == data.length - 1) {
                            $scope.start = data[i].metricsTime;
                        }
                    }
                }

                $timeout(function () {
                    myChart.setOption({
                        series: [{
                            data: $scope.data
                        }],
                        xAxis: [{
                            data: $scope.item
                        }]
                    });
                    myChart.resize();
                }, 50);
            });
        };

        $scope.$on('type', function (event, data) {
            $scope.loadData({size: 50, moId: data.host.id, type: 1, name: 'CPU_USED'});

            $interval(function () {
                $scope.loadData({size: 50, moId: data.host.id, type: 1, name: 'CPU_USED', start: $scope.start});
            }, 3000, 1000);
        });

    }])
    .controller('memMonitorController', ['$scope', '$timeout', 'MetricsResource', '$interval', function ($scope, $timeout, MetricsResource, $interval) {
        $scope.legend = '内存使用情况';
        $scope.item = [];
        $scope.data = [];

        var option = {
            // 提示框，鼠标悬浮交互时的信息提示
            tooltip: {
                trigger: 'axis',
                formatter: function (params) {
                    return params[0].name + '\n' + params[0].seriesName + ':' + params[0].value + 'MB';
                },
                axisPointer: {
                    animation: true
                }
            },
            // 横轴坐标轴
            xAxis: [{
                type: 'category',
                data: $scope.item,
                splitLine: {
                    show: false
                }
            }],
            // 纵轴坐标轴
            yAxis: [{
                type: 'value',
                // boundaryGap: [0, '100%'],
                splitLine: {
                    show: false
                }
            }],
            series: {
                name: $scope.legend,
                type: 'line',
                showSymbol: true,
                hoverAnimation: true
            }
        };
        var myChart = echarts.init(document.getElementById('mem'), 'macarons');

        myChart.setOption(option);

        $scope.start = '';

        $scope.loadData = function (param) {
            MetricsResource.query(param, function (data) {
                for (var i in data) {
                    if (i != '$promise' && i != '$resolved') {
                        if ($scope.data.length == 50) {
                            $scope.data.shift();
                            $scope.item.shift();
                        }
                        $scope.data.push(data[i].metricsValue / 100);
                        $scope.item.push(data[i].metricsTime);
                        if (i == data.length - 1) {
                            $scope.start = data[i].metricsTime;
                        }
                    }
                }

                $timeout(function () {
                    myChart.setOption({
                        series: [{
                            data: $scope.data
                        }],
                        xAxis: [{
                            data: $scope.item
                        }]
                    });
                    myChart.resize();
                }, 50);
            });

        };

        $scope.$on('type', function (event, data) {
            $scope.loadData({size: 50, moId: data.host.id, type: 1, name: 'MEM_USED'});

            $interval(function () {
                $scope.loadData({size: 50, moId: data.host.id, type: 1, name: 'MEM_USED', start: $scope.start});
            }, 3000, 1000);
        });

    }])
;
