/**
 * Created by zh on 17/3/7.
 */
angular.module('dockman.host.service', ['ngResource'])
    .factory('HostResource', ['$resource', function ($resource) {
        return $resource('/host/:id', {}, {
            'put': {
                method: 'PUT'
            },
            'gethost': {
                url: '/host/info/:id',
                method: 'GET'
            }
        })
    }]);