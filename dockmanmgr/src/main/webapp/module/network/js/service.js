/**
 * Created by zh on 17/3/17.
 */
angular.module('dockman.network.service', ['ngResource'])
    .factory('NetworkResource', ['$resource', function ($resource) {
        return $resource('/network/:id', {}, {
            'put': {
                method: 'PUT'
            }
        })
    }]);