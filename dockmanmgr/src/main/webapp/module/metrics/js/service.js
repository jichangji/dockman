/**
 * Created by zh on 17/3/17.
 */
angular.module('dockman.metrics.service', ['ngResource'])
    .factory('MetricsResource', ['$resource', function ($resource) {
        return $resource('/metrics/:id', {}, {
            'put': {
                method: 'PUT'
            }
        })
    }]);