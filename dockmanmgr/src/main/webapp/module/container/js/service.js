angular.module('dockman.container.service', ['ngResource'])
    .factory('ContainerResource', ['$resource', function ($resource) {
        return $resource('/container/:id', {}, {
            'put': {
                method: 'PUT'
            },
            'start': {
                url: '/container/start',
                method: 'POST',
                isArray: false
            },
            'stop': {
                url: '/container/stop',
                method: 'POST',
                isArray: false
            }
        })
    }]);