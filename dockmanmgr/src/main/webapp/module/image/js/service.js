angular.module('dockman.image.service', ['ngResource'])
    .factory('ImageResource', ['$resource', function ($resource) {
        return $resource('/image/:id', {}, {
            'put': {
                method: 'PUT'
            }
        })
    }]);