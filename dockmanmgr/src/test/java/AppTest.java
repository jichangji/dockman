import cn.qu.dockman.mgr.Application;
import cn.qu.dockman.mgr.entity.User;
import cn.qu.dockman.mgr.mapper.UserMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * Created by zh on 2017/3/5.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
public class AppTest {

    @Autowired
    private UserMapper mapper;

    @Test
    public void testInsert() {
        User user = new User();
        user.setUserName("张三");
        user.setAge(23);
        mapper.save(user);
        System.out.println("插入用户信息" + user.getUserName());
    }

    @Test
    public void testQuery() {
        Page<User> page = PageHelper.startPage(1, 2).doSelectPage(() -> mapper.queryAll());

        System.out.println(ToStringBuilder.reflectionToString(page));
    }
}
